package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.OrderEntry;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.*;
import com.gft.digitalbank.exchange.solution.ClosingCountDownLatch;
import com.gft.digitalbank.exchange.solution.message.matcher.Counter;
import com.gft.digitalbank.exchange.solution.message.matcher.Matcher;
import com.gft.digitalbank.exchange.solution.message.processor.Processor;
import com.google.common.collect.Sets;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Just some of the scenarios to run against the processor and have fast feedback.
 * <p>
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
@RunWith(JUnitParamsRunner.class)
public class TransactionEngineTest {

    @Parameters(method = "scenario17,scenario35,scenario36,scenario46")
    @Test
    public void shouldReturnProperTransactionAndOrderBookSets(List<BrokerMessage> messages, Set<Transaction> expectedTransactions, Set<OrderBook> expectedOrderBooks) {
        //given
        PriorityBlockingQueue<BrokerMessage> orders = new PriorityBlockingQueue<>(16, Comparator.comparingLong(BrokerMessage::getTimestamp));
        Processor processor = new Processor(orders, new Matcher(new Counter()), new ClosingCountDownLatch(1));
        TransactionEngine engine = new TransactionEngine(processor);

        //when
        orders.addAll(messages);

        //and
        engine.startProcessor();

        //then
        assertThat("Expected transaction set different from the current one", engine.getTransactions(), is(expectedTransactions));
        assertThat("Expected order book set different from the current one", engine.getOrderBooks(), is(expectedOrderBooks));
    }

    Object[] scenario17() {
        return new Object[]{
                new Object[]{Arrays.asList(
                        PositionOrder.builder().id(1).timestamp(1L).client("105").broker("3").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(2000).price(100).build()).build(),
                        PositionOrder.builder().id(4).timestamp(4L).client("103").broker("2").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(1000).price(100).build()).build(),
                        PositionOrder.builder().id(2).timestamp(2L).client("101").broker("2").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(200).price(100).build()).build(),
                        PositionOrder.builder().id(3).timestamp(3L).client("102").broker("1").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(300).price(90).build()).build(),
                        PositionOrder.builder().id(5).timestamp(5L).client("100").broker("1").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(400).price(110).build()).build()),

                        Sets.newHashSet(
                                Transaction.builder().id(1).amount(200).price(100).brokerBuy("2").brokerSell("3").clientBuy("101")
                                        .clientSell("105").product("A").build(),
                                Transaction.builder().id(2).amount(1000).price(100).brokerBuy("2").brokerSell("3").clientBuy("103")
                                        .clientSell("105").product("A").build(),
                                Transaction.builder().id(3).amount(400).price(100).brokerBuy("1").brokerSell("3").clientBuy("100")
                                        .clientSell("105").product("A").build()),

                        Sets.newHashSet(
                                OrderBook.builder().product("A")
                                        .sellEntry(OrderEntry.builder().id(1).amount(400).price(100).broker("3").client("105").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(300).price(90).broker("1").client("102").build())
                                        .build())
                }};
    }

    Object[] scenario35() {
        return new Object[]{
                new Object[]{Arrays.asList(
                        PositionOrder.builder().id(1).timestamp(1L).client("100").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(100).price(199).build()).build(),
                        PositionOrder.builder().id(2).timestamp(2L).client("101").broker("2").side(Side.BUY).product("B")
                                .details(OrderDetails.builder().amount(200).price(111).build()).build(),

                        ModificationOrder.builder().id(14).timestamp(14L).broker("4").modifiedOrderId(4)
                                .details(OrderDetails.builder().amount(500).price(210).build()).build(),

                        PositionOrder.builder().id(6).timestamp(6L).client("105").broker("3").side(Side.SELL).product("B")
                                .details(OrderDetails.builder().amount(200).price(141).build()).build(),
                        PositionOrder.builder().id(3).timestamp(3L).client("102").broker("3").side(Side.SELL).product("C")
                                .details(OrderDetails.builder().amount(300).price(144).build()).build(),
                        PositionOrder.builder().id(4).timestamp(4L).client("103").broker("4").side(Side.BUY).product("D")
                                .details(OrderDetails.builder().amount(400).price(150).build()).build(),
                        PositionOrder.builder().id(5).timestamp(5L).client("104").broker("4").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(100).price(190).build()).build(),
                        PositionOrder.builder().id(13).timestamp(13L).client("108").broker("4").side(Side.SELL).product("D")
                                .details(OrderDetails.builder().amount(400).price(211).build()).build(),
                        PositionOrder.builder().id(7).timestamp(7L).client("106").broker("2").side(Side.BUY).product("C")
                                .details(OrderDetails.builder().amount(300).price(140).build()).build(),
                        PositionOrder.builder().id(8).timestamp(8L).client("107").broker("1").side(Side.SELL).product("D")
                                .details(OrderDetails.builder().amount(400).price(155).build()).build(),

                        ModificationOrder.builder().id(11).timestamp(11L).broker("3").modifiedOrderId(3)
                                .details(OrderDetails.builder().amount(200).price(100).build()).build(),
                        ModificationOrder.builder().id(9).timestamp(9L).broker("1").modifiedOrderId(1)
                                .details(OrderDetails.builder().amount(200).price(100).build()).build(),
                        ModificationOrder.builder().id(10).timestamp(10L).broker("2").modifiedOrderId(2)
                                .details(OrderDetails.builder().amount(200).price(151).build()).build(),

                        PositionOrder.builder().id(12).timestamp(12L).client("108").broker("4").side(Side.SELL).product("D")
                                .details(OrderDetails.builder().amount(50).price(205).build()).build()),

                        Sets.newHashSet(Transaction.builder().id(1).amount(100).price(190).brokerBuy("4").brokerSell("1").clientBuy("104")
                                        .clientSell("100").product("A").build(),
                                Transaction.builder().id(1).amount(200).price(141).brokerBuy("2").brokerSell("3").clientBuy("101")
                                        .clientSell("105").product("B").build(),
                                Transaction.builder().id(1).amount(200).price(140).brokerBuy("2").brokerSell("3").clientBuy("106")
                                        .clientSell("102").product("C").build(),
                                Transaction.builder().id(1).amount(400).price(155).brokerBuy("4").brokerSell("1").clientBuy("103")
                                        .clientSell("107").product("D").build(),
                                Transaction.builder().id(2).amount(50).price(205).brokerBuy("4").brokerSell("4").clientBuy("103")
                                        .clientSell("108").product("D").build()),

                        Sets.newHashSet(OrderBook.builder().product("A")
                                        .sellEntry(OrderEntry.builder().id(1).amount(100).price(100).broker("1").client("100").build())
                                        .build(),
                                OrderBook.builder().product("D")
                                        .sellEntry(OrderEntry.builder().id(1).amount(400).price(211).broker("4").client("108").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(50).price(210).broker("4").client("103").build())
                                        .build(),
                                OrderBook.builder().product("C")
                                        .buyEntry(OrderEntry.builder().id(1).amount(100).price(140).broker("2").client("106").build())
                                        .build())
                }};
    }

    Object[] scenario36() {
        return new Object[]{
                new Object[]{Arrays.asList(
                        PositionOrder.builder().id(1).timestamp(1L).client("100").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(100).price(100).build()).build(),
                        PositionOrder.builder().id(2).timestamp(2L).client("101").broker("2").side(Side.BUY).product("B")
                                .details(OrderDetails.builder().amount(200).price(100).build()).build(),
                        PositionOrder.builder().id(3).timestamp(3L).client("102").broker("1").side(Side.SELL).product("C")
                                .details(OrderDetails.builder().amount(300).price(100).build()).build(),
                        PositionOrder.builder().id(4).timestamp(4L).client("103").broker("3").side(Side.BUY).product("D")
                                .details(OrderDetails.builder().amount(400).price(100).build()).build(),
                        PositionOrder.builder().id(5).timestamp(5L).client("104").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(200).price(100).build()).build(),
                        PositionOrder.builder().id(6).timestamp(6L).client("105").broker("2").side(Side.BUY).product("B")
                                .details(OrderDetails.builder().amount(300).price(100).build()).build(),
                        PositionOrder.builder().id(11).timestamp(11L).client("110").broker("1").side(Side.SELL).product("C")
                                .details(OrderDetails.builder().amount(300).price(95).build()).build(),
                        PositionOrder.builder().id(12).timestamp(12L).client("111").broker("3").side(Side.BUY).product("D")
                                .details(OrderDetails.builder().amount(400).price(105).build()).build(),
                        PositionOrder.builder().id(7).timestamp(7L).client("106").broker("1").side(Side.SELL).product("C")
                                .details(OrderDetails.builder().amount(400).price(100).build()).build(),
                        PositionOrder.builder().id(8).timestamp(8L).client("107").broker("3").side(Side.BUY).product("D")
                                .details(OrderDetails.builder().amount(500).price(100).build()).build(),

                        ModificationOrder.builder().id(17).timestamp(17L).broker("1").modifiedOrderId(13)
                                .details(OrderDetails.builder().amount(100).price(91).build()).build(),

                        PositionOrder.builder().id(9).timestamp(9L).client("108").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(100).price(95).build()).build(),
                        PositionOrder.builder().id(10).timestamp(10L).client("109").broker("2").side(Side.BUY).product("B")
                                .details(OrderDetails.builder().amount(200).price(105).build()).build(),
                        PositionOrder.builder().id(13).timestamp(13L).client("112").broker("1").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(100).price(90).build()).build(),
                        PositionOrder.builder().id(14).timestamp(14L).client("113").broker("2").side(Side.SELL).product("B")
                                .details(OrderDetails.builder().amount(200).price(110).build()).build(),

                        CancellationOrder.builder().id(20).timestamp(20L).cancelledOrderId(12).broker("3").build(),

                        PositionOrder.builder().id(15).timestamp(15L).client("114").broker("1").side(Side.BUY).product("C")
                                .details(OrderDetails.builder().amount(300).price(90).build()).build(),
                        PositionOrder.builder().id(16).timestamp(16L).client("115").broker("3").side(Side.SELL).product("D")
                                .details(OrderDetails.builder().amount(400).price(110).build()).build(),

                        ModificationOrder.builder().id(18).timestamp(18L).broker("2").modifiedOrderId(10)
                                .details(OrderDetails.builder().amount(200).price(99).build()).build(),
                        CancellationOrder.builder().id(19).timestamp(19L).cancelledOrderId(3).broker("1").build()),

                        Collections.emptySet(),

                        Sets.newHashSet(OrderBook.builder().product("A")
                                        .sellEntry(OrderEntry.builder().id(1).amount(100).price(95).broker("1").client("108").build())
                                        .sellEntry(OrderEntry.builder().id(2).amount(100).price(100).broker("1").client("100").build())
                                        .sellEntry(OrderEntry.builder().id(3).amount(200).price(100).broker("1").client("104").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(100).price(91).broker("1").client("112").build())
                                        .build(),
                                OrderBook.builder().product("B")
                                        .sellEntry(OrderEntry.builder().id(1).amount(200).price(110).broker("2").client("113").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(200).price(100).broker("2").client("101").build())
                                        .buyEntry(OrderEntry.builder().id(2).amount(300).price(100).broker("2").client("105").build())
                                        .buyEntry(OrderEntry.builder().id(3).amount(200).price(99).broker("2").client("109").build())
                                        .build(),
                                OrderBook.builder().product("C")
                                        .sellEntry(OrderEntry.builder().id(1).amount(300).price(95).broker("1").client("110").build())
                                        .sellEntry(OrderEntry.builder().id(2).amount(400).price(100).broker("1").client("106").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(300).price(90).broker("1").client("114").build())
                                        .build(),
                                OrderBook.builder().product("D")
                                        .sellEntry(OrderEntry.builder().id(1).amount(400).price(110).broker("3").client("115").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(400).price(100).broker("3").client("103").build())
                                        .buyEntry(OrderEntry.builder().id(2).amount(500).price(100).broker("3").client("107").build())
                                        .build())
                }};
    }

    Object[] scenario46() {
        return new Object[]{
                new Object[]{Arrays.asList(

                        PositionOrder.builder().id(2).timestamp(2L).client("101").broker("2").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(20000000).price(3).build()).build(),
                        PositionOrder.builder().id(1).timestamp(1L).client("100").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(1000000).price(5).build()).build(),
                        PositionOrder.builder().id(6).timestamp(6L).client("103").broker("1").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(10000000).price(4).build()).build(),
                        PositionOrder.builder().id(3).timestamp(3L).client("102").broker("1").side(Side.SELL).product("A")
                                .details(OrderDetails.builder().amount(300000).price(4).build()).build(),
                        CancellationOrder.builder().id(4).timestamp(4L).cancelledOrderId(2).broker("2").build(),
                        ModificationOrder.builder().id(5).timestamp(5L).broker("1").modifiedOrderId(1)
                                .details(OrderDetails.builder().amount(200000000).price(6).build()).build(),
                        PositionOrder.builder().id(7).timestamp(7L).client("104").broker("3").side(Side.BUY).product("A")
                                .details(OrderDetails.builder().amount(20000000).price(10).build()).build()),

                        Sets.newHashSet(Transaction.builder().id(1).amount(300000).price(4).brokerBuy("1").brokerSell("1").clientBuy("103")
                                        .clientSell("102").product("A").build(),
                                Transaction.builder().id(2).amount(20000000).price(6).brokerBuy("3").brokerSell("1")
                                        .clientBuy("104").clientSell("100").product("A").build()),

                        Sets.newHashSet(
                                OrderBook.builder().product("A")
                                        .sellEntry(OrderEntry.builder().id(1).amount(180000000).price(6).client("100").broker("1").build())
                                        .buyEntry(OrderEntry.builder().id(1).amount(9700000).price(4).client("103").broker("1").build())
                                        .build())}};
    }

}