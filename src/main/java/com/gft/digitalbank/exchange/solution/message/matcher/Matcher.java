package com.gft.digitalbank.exchange.solution.message.matcher;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.OrderEntry;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.google.common.collect.ImmutableSet;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static com.gft.digitalbank.exchange.model.orders.Side.BUY;
import static com.gft.digitalbank.exchange.model.orders.Side.SELL;
import static com.gft.digitalbank.exchange.solution.util.GuavaCollectors.toImmutableList;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class Matcher {

    private final Counter counter;

    public Matcher(Counter counter) {
        Assert.notNull(counter, "Transaction counter cannot be null");
        this.counter = counter;
    }

    public Set<Transaction> getTransactionFor(PositionOrder order, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        Set<Transaction> transactions = new HashSet<>();

        if (SELL == order.getSide()) {
            Set<PositionOrder> buys = ordersBook.get(order.getProduct()).get(BUY);
            if (buys != null) {
                Set<PositionOrder> sells = ordersBook.get(order.getProduct()).get(SELL);
                transactions = getTransactionsFor(order, sells, buys);
            }
        } else if (BUY == order.getSide()) {
            Set<PositionOrder> sells = ordersBook.get(order.getProduct()).get(SELL);
            if (sells != null) {
                Set<PositionOrder> buys = ordersBook.get(order.getProduct()).get(BUY);
                transactions = getTransactionsFor(order, buys, sells);
            }
        }
        System.out.println("Returning transactions to be added: " + transactions);
        return transactions;
    }

    public ImmutableSet<OrderBook> getOrderBooksFor(Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        ImmutableSet.Builder<OrderBook> builder = ImmutableSet.builder();
        for (String product : ordersBook.keySet()) {
            List<OrderEntry> productBuys = getOrderEntriesFor(product, BUY, ordersBook.get(product).get(BUY));
            List<OrderEntry> productSells = getOrderEntriesFor(product, SELL, ordersBook.get(product).get(SELL));
            if (!productBuys.isEmpty() || !productSells.isEmpty()) {
                builder.add(OrderBook.builder().product(product).buyEntries(productBuys).sellEntries(productSells).build());
            }
        }
        return builder.build();
    }

    private List<OrderEntry> getOrderEntriesFor(String product, Side side, Set<PositionOrder> orders) {
        if (!CollectionUtils.isEmpty(orders)) {
            return orders.stream().map(order -> new OrderEntry(counter.getNextFor(product, side), order.getBroker(), order.getDetails().getAmount(), order.getDetails().getPrice(), order.getClient())).collect(toImmutableList());
        }
        return Collections.emptyList();
    }

    private Set<Transaction> getTransactionsFor(PositionOrder order, Set<PositionOrder> orderSet, Set<PositionOrder> opposedOrders) {
        Set<Transaction> transactions = new HashSet<>();

        Set<PositionOrder> newOpposedOrders = new HashSet<>();
        Iterator<PositionOrder> it = opposedOrders.iterator();
        while (it.hasNext()) {
            Set<PositionOrder> newOrders = new HashSet<>();
            PositionOrder current = it.next();
            if (offersMatch(order, current)) {
                transactions.add(getTransactionFor(order, current));
                it.remove();
                orderSet.remove(order);
                if (isTransactionFullyExecuted(order, current)) {
                    break;
                }
                if (isOrderFullyExecuted(order, current)) {
                    getNewOrderFrom(order, current, newOpposedOrders);
                    break;
                }
                if (isOrderPartiallyExecuted(order, current)) {
                    order = getNewOrderFrom(current, order, newOrders);
                }
            }
            orderSet.addAll(newOrders);
        }
        opposedOrders.addAll(newOpposedOrders);
        return transactions;
    }

    private Transaction getTransactionFor(PositionOrder order, PositionOrder current) {
        switch (order.getSide()) {
            case SELL:
                return Transaction.builder().id(counter.getNextFor(order.getProduct())).price(current.getDetails().getPrice()).amount(getAmountFor(current.getDetails().getAmount(), order.getDetails().getAmount())).brokerBuy(current.getBroker()).brokerSell(order.getBroker()).clientBuy(current.getClient()).clientSell(order.getClient()).product(order.getProduct()).build();
            case BUY:
                return Transaction.builder().id(counter.getNextFor(order.getProduct())).price(current.getDetails().getPrice()).amount(getAmountFor(order.getDetails().getAmount(), current.getDetails().getAmount())).brokerBuy(order.getBroker()).brokerSell(current.getBroker()).clientBuy(order.getClient()).clientSell(current.getClient()).product(order.getProduct()).build();
            default:
                throw new RuntimeException("Unknown side");
        }
    }

    private boolean offersMatch(PositionOrder order, PositionOrder current) {
        switch (order.getSide()) {
            case BUY:
                return current.getDetails().getPrice() <= order.getDetails().getPrice();
            case SELL:
                return current.getDetails().getPrice() >= order.getDetails().getPrice();
            default:
                throw new RuntimeException("Unknown side");
        }
    }

    private PositionOrder getNewOrderFrom(PositionOrder order, PositionOrder current, Set<PositionOrder> newOrdersSet) {
        OrderDetails details = OrderDetails.builder().amount(current.getDetails().getAmount() - order.getDetails().getAmount()).price(current.getDetails().getPrice()).build();
        PositionOrder newOrder = PositionOrder.builder().broker(current.getBroker()).client(current.getClient()).details(details).id(current.getId()).product(current.getProduct()).side(current.getSide()).timestamp(current.getTimestamp()).build();
        newOrdersSet.add(newOrder);
        return newOrder;
    }

    private boolean isOrderPartiallyExecuted(PositionOrder order, PositionOrder current) {
        return current.getDetails().getAmount() < order.getDetails().getAmount();
    }

    private boolean isOrderFullyExecuted(PositionOrder order, PositionOrder current) {
        return current.getDetails().getAmount() > order.getDetails().getAmount();
    }

    private boolean isTransactionFullyExecuted(PositionOrder order, PositionOrder current) {
        return current.getDetails().getAmount() == order.getDetails().getAmount();
    }

    private int getAmountFor(int buy, int sell) {
        return buy > sell ? sell : buy;
    }

}
