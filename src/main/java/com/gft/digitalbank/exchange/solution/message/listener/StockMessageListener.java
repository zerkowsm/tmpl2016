package com.gft.digitalbank.exchange.solution.message.listener;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.MessageType;
import com.gft.digitalbank.exchange.solution.BootingCountDownLatch;
import com.gft.digitalbank.exchange.solution.ClosingCountDownLatch;
import com.gft.digitalbank.exchange.solution.message.MessageFactory;
import lombok.extern.slf4j.Slf4j;

import javax.jms.*;
import java.util.concurrent.BlockingQueue;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
@Slf4j
public class StockMessageListener implements MessageListener {

	private final ConnectionFactory connectionFactory;
	private final String queueName;

	private final BootingCountDownLatch start;
	private final ClosingCountDownLatch stop;

	private final BlockingQueue<BrokerMessage> orders;

	private Connection connection;
	private Session session;
	private MessageConsumer consumer;

	public StockMessageListener(ConnectionFactory connectionFactory, String destination, BootingCountDownLatch start, ClosingCountDownLatch stop, BlockingQueue<BrokerMessage> orders) {
		this.connectionFactory = connectionFactory;
		this.queueName = destination;
		this.start = start;
		this.stop = stop;
		this.orders = orders;
	}

	void start() throws JMSException {
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
		consumer = session.createConsumer(session.createQueue(queueName));
		consumer.setMessageListener(this);
		connection.start();
		start.reportStart();
		log.debug("Listener started");
	}

	private void stop() {
		try {
			consumer.close();
			session.close();
			connection.close();
		} catch (JMSException e) {
			throw new RuntimeException("Listener failed to stop");
		} finally {
			stop.reportStop();
		}
		log.debug("Listener stopped");
	}

	@Override
	public void onMessage(Message message) {
		try {
			message.acknowledge();
			if (isShutdown(message)) {
				System.out.println("Shutdown: " + message.getStringProperty("messageType"));
				stop();
			} else {
				sendForProcessing((TextMessage) message);
			}
		} catch (JMSException e) {
			throw new RuntimeException("Error during receiving message");
		}
	}

	private boolean isShutdown(Message message) throws JMSException {
		return MessageType.SHUTDOWN_NOTIFICATION.toString().equals(message.getStringProperty("messageType"));
	}

	private void sendForProcessing(TextMessage textMessage) throws JMSException {
		log.debug("Sending msg to be processed:\n      " + textMessage.getText());
		orders.offer(MessageFactory.from(textMessage));
		System.out.println("sent: " + textMessage.getText());
	}

}
