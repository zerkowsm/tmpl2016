package com.gft.digitalbank.exchange.solution.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.function.Function;
import java.util.stream.Collector;

import static java.util.stream.Collector.Characteristics.UNORDERED;

/**
 * Heavily inspired by https://gist.github.com/JakeWharton/9734167 ;)
 * <p>
 * Created by zerkowski.maciek@gmail.com on 2016-07-19
 */
public class GuavaCollectors {

    /**
     * Collect a stream of elements into an {@link ImmutableList}
     */
    public static <T> Collector<T, ImmutableList.Builder<T>, ImmutableList<T>> toImmutableList() {
        return Collector.of(ImmutableList.Builder<T>::new,
                ImmutableList.Builder<T>::add,
                (l, r) -> l.addAll(r.build()),
                ImmutableList.Builder<T>::build);
    }

    /**
     * Collect a stream of elements into an {@link ImmutableSet}
     */
    public static <T> Collector<T, ImmutableSet.Builder<T>, ImmutableSet<T>> toImmutableSet() {
        return Collector.of(ImmutableSet.Builder<T>::new,
                ImmutableSet.Builder<T>::add,
                (l, r) -> l.addAll(r.build()),
                ImmutableSet.Builder<T>::build, UNORDERED);
    }

    /**
     * Collect a stream of elements into an {@link ImmutableMap}
     */
    public static <T, K, V> Collector<T, ImmutableMap.Builder<K, V>, ImmutableMap<K, V>> toImmutableMap(Function<? super T, ? extends K> keyMapper,
                                                                                                        Function<? super T, ? extends V> valueMapper) {
        return Collector.of(ImmutableMap.Builder<K, V>::new,
                (r, t) -> r.put(keyMapper.apply(t), valueMapper.apply(t)),
                (l, r) -> l.putAll(r.build()),
                ImmutableMap.Builder::build,
                Collector.Characteristics.UNORDERED);
    }

}
