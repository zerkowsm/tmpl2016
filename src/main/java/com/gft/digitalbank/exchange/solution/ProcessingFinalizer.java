package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.SolutionResult;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.engine.TransactionEngine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.util.Set;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
@Slf4j
public class ProcessingFinalizer implements Runnable {

    private final TransactionEngine transactionEngine;
    private final ProcessingListener processingListener;
    private final ClosingCountDownLatch stop;

    public ProcessingFinalizer(TransactionEngine transactionEngine, ProcessingListener processingListener, ClosingCountDownLatch stop) {
        Assert.notNull(transactionEngine, "Transaction engine cannot be null");
        Assert.notNull(processingListener, "Processing listener cannot be null");
        Assert.notNull(stop, "Closing count down latch cannot be null");
        this.transactionEngine = transactionEngine;
        this.processingListener = processingListener;
        this.stop = stop;
    }

    @Override
    public void run() {
        try {
            log.debug("Waiting until all messages are processed...");

            stop.awaitUntilFullyStopped();

            log.debug("All seems to be processed, preparing results");

            Set<Transaction> trans = transactionEngine.getTransactions();
            System.out.println("trans: " + trans);
            Set<OrderBook> orders = transactionEngine.getOrderBooks();
            System.out.println("orders: " + orders);

            processingListener.processingDone(new SolutionResult(trans, orders));
        } catch (InterruptedException e) {
            throw new RuntimeException("Thread interrupted");
        }
    }

}