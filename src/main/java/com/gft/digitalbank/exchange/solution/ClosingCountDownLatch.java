package com.gft.digitalbank.exchange.solution;

import java.util.concurrent.CountDownLatch;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class ClosingCountDownLatch {

	private final CountDownLatch stop;

	public ClosingCountDownLatch(int threadsNumber) {
		this.stop = new CountDownLatch(threadsNumber);
	}

	public void reportStop() {
		stop.countDown();
	}

	public void awaitUntilFullyStopped() throws InterruptedException {
		stop.await();
	}

	public boolean isOneStopReportMissing() {
		return stop.getCount() == 1;
	}

}
