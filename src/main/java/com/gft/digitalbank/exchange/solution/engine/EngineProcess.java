package com.gft.digitalbank.exchange.solution.engine;

import org.springframework.util.Assert;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class EngineProcess implements Runnable {

	private final TransactionEngine transactionEngine;

	public EngineProcess(TransactionEngine transactionEngine) {
		Assert.notNull(transactionEngine, "Transaction engine cannot be null");
		this.transactionEngine = transactionEngine;
	}

	@Override
	public void run() {
		transactionEngine.startProcessor();
	}

}