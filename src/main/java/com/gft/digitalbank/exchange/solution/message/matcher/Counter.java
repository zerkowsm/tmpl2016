package com.gft.digitalbank.exchange.solution.message.matcher;

import com.gft.digitalbank.exchange.model.orders.Side;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Simple counter. Not used in multithreaded environment
 * <p>
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class Counter {

	private final Map<String, AtomicInteger> transactionsCounter = new HashMap<>();
	private final Map<String, Map<Side, AtomicInteger>> ordersBookCounter = new HashMap<>();

	public int getNextFor(String product) {
		AtomicInteger counterPerProduct = transactionsCounter.get(product);
		if (counterPerProduct == null) {
			AtomicInteger counter = new AtomicInteger();
			transactionsCounter.put(product, counter);
			return counter.incrementAndGet();
		} else {
			return counterPerProduct.incrementAndGet();
		}
	}

	public int getNextFor(String product, Side side) {
		Map<Side, AtomicInteger> counterPerProductSide = ordersBookCounter.get(product);
		if (counterPerProductSide == null) {
			AtomicInteger counter = new AtomicInteger();
			Map<Side, AtomicInteger> counterPerSide = new HashMap<>();
			counterPerSide.put(side, counter);
			ordersBookCounter.put(product, counterPerSide);
			return counter.incrementAndGet();
		} else if (counterPerProductSide.get(side) == null) {
			AtomicInteger counter = new AtomicInteger();
			Map<Side, AtomicInteger> counterPerSide = new HashMap<>();
			counterPerSide.put(side, counter);
			ordersBookCounter.put(product, counterPerSide);
			return counter.incrementAndGet();
		} else {
			return counterPerProductSide.get(side).incrementAndGet();
		}
	}

}
