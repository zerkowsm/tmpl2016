package com.gft.digitalbank.exchange.solution.message;

import com.gft.digitalbank.exchange.model.orders.*;
import com.google.gson.Gson;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import static com.gft.digitalbank.exchange.model.orders.MessageType.*;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class MessageFactory {

    public static BrokerMessage from(TextMessage jsonTextMessage) throws JMSException {
        Gson gson = new Gson();
        MessageType msgType = valueOf(jsonTextMessage.getStringProperty("messageType"));

        switch (msgType) {
            case ORDER:
                return gson.fromJson(jsonTextMessage.getText(), PositionOrder.class);
            case MODIFICATION:
                return gson.fromJson(jsonTextMessage.getText(), ModificationOrder.class);
            case CANCEL:
                return gson.fromJson(jsonTextMessage.getText(), CancellationOrder.class);
            case SHUTDOWN_NOTIFICATION:
                return gson.fromJson(jsonTextMessage.getText(), ShutdownNotification.class);
            default:
                throw new RuntimeException("Unknown message type");
        }
    }

}
