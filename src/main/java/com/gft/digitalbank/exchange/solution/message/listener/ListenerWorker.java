package com.gft.digitalbank.exchange.solution.message.listener;

import org.springframework.util.Assert;

import javax.jms.JMSException;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class ListenerWorker implements Runnable {

	private final StockMessageListener stockMessageListener;

	public ListenerWorker(StockMessageListener stockMessageListener) {
		Assert.notNull(stockMessageListener, "Stock message listener cannot be null");
		this.stockMessageListener = stockMessageListener;
	}

	@Override
	public void run() {
		try {
			stockMessageListener.start();
		} catch (JMSException e) {
			throw new RuntimeException("Listener failed to startProcessor");
		}
	}

}
