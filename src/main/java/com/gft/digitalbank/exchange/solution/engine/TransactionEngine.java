package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.message.processor.Processor;
import org.springframework.util.Assert;

import java.util.Set;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class TransactionEngine {

	private final Processor processor;

	public TransactionEngine(Processor processor) {
		Assert.notNull(processor, "Processor cannot be null");
		this.processor = processor;
	}

	public void startProcessor() {
		processor.doProcess();
	}

	public Set<Transaction> getTransactions() {
		return processor.getTransactions();
	}

	public Set<OrderBook> getOrderBooks() {
		return processor.getOrderBooks();
	}

}
