package com.gft.digitalbank.exchange.solution.message.processor;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.*;
import com.gft.digitalbank.exchange.solution.ClosingCountDownLatch;
import com.gft.digitalbank.exchange.solution.message.matcher.Matcher;
import com.google.common.collect.ImmutableSet;
import javaslang.collection.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;

import static com.gft.digitalbank.exchange.model.orders.Side.SELL;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
@Slf4j
public class Processor {

    private final Set<Transaction> transactions = new HashSet<>();
    private final Map<String, Map<Side, Set<PositionOrder>>> ordersBook = new HashMap<>();

    private final BlockingQueue<BrokerMessage> orders;
    private final Matcher matcher;
    private final ClosingCountDownLatch stop;

    private ImmutableSet<Transaction> transactionImmutableSet;
    private ImmutableSet<OrderBook> orderBookImmutableSet;

    public Processor(BlockingQueue<BrokerMessage> orders, Matcher matcher, ClosingCountDownLatch stop) {
        Assert.notNull(orders, "Orders queue cannot be null");
        Assert.notNull(matcher, "Matcher cannot be null");
        Assert.notNull(stop, "Closing count down latch queue cannot be null");
        this.orders = orders;
        this.matcher = matcher;
        this.stop = stop;
    }

    public void doProcess() {
        long prevTimeStamp = 0;
        while (true) {
            BrokerMessage msg;
            try {
                msg = orders.poll(100, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException("Thread interrupted");
            }
            System.out.println("polled msg: " + msg);
            if (msg != null) {
                if (isMessageNextToBeProcessed(msg, prevTimeStamp)) {
                    log.debug("Processing: " + msg.getTimestamp());
                    prevTimeStamp = msg.getTimestamp();
                    log.debug("Next expected timestamp: " + (msg.getTimestamp() + 1));

                    switch (msg.getMessageType()) {
                        case ORDER:
                            processPositionOrder((PositionOrder) msg, ordersBook);
                            break;
                        case MODIFICATION:
                            processModificationOrder((ModificationOrder) msg, ordersBook);
                            break;
                        case CANCEL:
                            processCancellationOrder((CancellationOrder) msg, ordersBook);
                            break;
                        default:
                            throw new RuntimeException("Unknown message type");
                    }
                } else {
                    log.debug("Sending back to reprocess: " + msg.getTimestamp());
                    orders.offer(msg);
                }
            } else if (stop.isOneStopReportMissing()) {
                log.debug("SIZE: " + transactions.size());
                log.debug("Listeners already closed and no more orders to be processed - stopping processing");
                stop.reportStop();
                break;
            }
        }
    }

    private void processPositionOrder(PositionOrder order, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        transactions.addAll(matcher.getTransactionFor(order, addOrderToOrdersBook(order, ordersBook)));
    }

    private void processCancellationOrder(CancellationOrder cancellationOrder, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        ordersBook.entrySet()
                .stream()
                .filter(books -> books.getValue().entrySet()
                        .stream()
                        .filter(entry -> orderCancelled(cancellationOrder, entry.getValue()))
                        .findFirst()
                        .isPresent())
                .findFirst();
    }

    private void processModificationOrder(ModificationOrder modificationOrder, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        ordersBook.entrySet()
                .stream()
                .filter(books -> books.getValue().entrySet()
                        .stream()
                        .filter(entry -> orderModified(modificationOrder, entry.getValue()))
                        .findFirst()
                        .isPresent())
                .findFirst();
    }

    private Map<String, Map<Side, Set<PositionOrder>>> addOrderToOrdersBook(PositionOrder order, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        if (ordersBook.get(order.getProduct()) == null) {
            prepareProductOrders(order, ordersBook);
        } else if (ordersBook.get(order.getProduct()).get(order.getSide()) == null) {
            prepareProductOrderSet(order, ordersBook);
        } else {
            ordersBook.get(order.getProduct()).get(order.getSide()).add(order);
        }
        return ordersBook;
    }

    private void prepareProductOrderSet(PositionOrder order, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        Set<PositionOrder> orders = getOrderSetFor(order);
        orders.add(order);
        ordersBook.get(order.getProduct()).put(order.getSide(), orders);
    }

    private void prepareProductOrders(PositionOrder order, Map<String, Map<Side, Set<PositionOrder>>> ordersBook) {
        Map<Side, Set<PositionOrder>> siteOrders = new HashMap<>();
        Set<PositionOrder> orders = getOrderSetFor(order);
        orders.add(order);
        siteOrders.put(order.getSide(), orders);
        ordersBook.put(order.getProduct(), siteOrders);
    }

    private boolean isMessageNextToBeProcessed(BrokerMessage msg, long prevTimeStamp) {
        return prevTimeStamp + 1 >= msg.getTimestamp();
    }

    private Set<PositionOrder> getOrderSetFor(PositionOrder order) {
        if (SELL == order.getSide()) {
            return new ConcurrentSkipListSet<>(Comparator.comparingInt((PositionOrder e) -> e.getDetails().getPrice()).thenComparingLong(BrokerMessage::getTimestamp));
        } else {
            return new ConcurrentSkipListSet<>(Comparator.comparingInt((PositionOrder e) -> e.getDetails().getPrice()).reversed().thenComparingLong(BrokerMessage::getTimestamp));
        }
    }

    private boolean orderCancelled(CancellationOrder order, Set<PositionOrder> orders) {
//       return orders
//                .stream()
//                .peek(e -> orders
//                        .removeIf(current -> current.getId() == order.getCancelledOrderId() && Objects.equals(current.getBroker(), order.getBroker())))
//                .findFirst()
//                .isPresent();

        Iterator<PositionOrder> it = orders.iterator();
        while (it.hasNext()) {
            PositionOrder current = it.next();
            if (current.getId() == order.getCancelledOrderId() && Objects.equals(current.getBroker(), order.getBroker())) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    private boolean orderModified(ModificationOrder modificationOrder, Set<PositionOrder> orders) {
        PositionOrder newOrder = getModifiedOrderFor(modificationOrder, orders);
        if (newOrder != null) {
            this.orders.add(newOrder);
            return true;
        }
        return false;
    }

    private PositionOrder getModifiedOrderFor(ModificationOrder order, Set<PositionOrder> orders) {
        Iterator<PositionOrder> it = orders.iterator();
        while (it.hasNext()) {
            PositionOrder current = it.next();
            if (current.getId() == order.getModifiedOrderId() && Objects.equals(current.getBroker(), order.getBroker())) {
                OrderDetails details = OrderDetails.builder().amount(order.getDetails().getAmount()).price(order.getDetails().getPrice()).build();
                PositionOrder newOrder = PositionOrder.builder().broker(current.getBroker()).client(current.getClient()).details(details).id(current.getId()).product(current.getProduct()).side(current.getSide()).timestamp(order.getTimestamp()).build();
                it.remove();
                return newOrder;
            }
        }
        return null;
    }

    public Set<Transaction> getTransactions() {
        ImmutableSet<Transaction> transactions = transactionImmutableSet;
        return (transactions == null) ? transactionImmutableSet = ImmutableSet.copyOf(this.transactions) : transactionImmutableSet;
    }

    public Set<OrderBook> getOrderBooks() {
        ImmutableSet<OrderBook> orderBooks = orderBookImmutableSet;
        return (orderBooks == null) ? orderBookImmutableSet = matcher.getOrderBooksFor(ordersBook) : orderBookImmutableSet;
    }

}
