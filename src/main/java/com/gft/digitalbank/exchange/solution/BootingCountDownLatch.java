package com.gft.digitalbank.exchange.solution;

import java.util.concurrent.CountDownLatch;

/**
 * Created by zerkowski.maciek@gmail.com on 2016-07-17
 */
public class BootingCountDownLatch {

	private final CountDownLatch start;

	public BootingCountDownLatch(int threadsNumber) {
		this.start = new CountDownLatch(threadsNumber);
	}

	public void reportStart() {
		start.countDown();
	}

	public void awaitUntilFullyBooted() throws InterruptedException {
		start.await();
	}

}
