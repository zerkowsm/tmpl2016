package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.engine.EngineProcess;
import com.gft.digitalbank.exchange.solution.engine.TransactionEngine;
import com.gft.digitalbank.exchange.solution.message.listener.ListenerWorker;
import com.gft.digitalbank.exchange.solution.message.listener.StockMessageListener;
import com.gft.digitalbank.exchange.solution.message.matcher.Counter;
import com.gft.digitalbank.exchange.solution.message.matcher.Matcher;
import com.gft.digitalbank.exchange.solution.message.processor.Processor;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Your solution must implement the {@link Exchange} interface.
 */
public class StockExchange implements Exchange {

    private final BlockingQueue<BrokerMessage> orders = new PriorityBlockingQueue<>(16, Comparator.comparingLong(BrokerMessage::getTimestamp));
    private ProcessingListener processingListener;
    private ClosingCountDownLatch stop;

    @Override
    public void register(ProcessingListener processingListener) {
        this.processingListener = processingListener;
    }

    @Override
    public void setDestinations(List<String> list) {
        int destinationsNumber = list.size();

        BootingCountDownLatch start = new BootingCountDownLatch(destinationsNumber);
        stop = new ClosingCountDownLatch(destinationsNumber + 1);

        ConnectionFactory connectionFactory;
        try {
            Context context = new InitialContext();
            connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
        } catch (NamingException e) {
            throw new RuntimeException("Connection Factory initialization failed");
        }

        list.stream().forEach(destination -> new Thread(new ListenerWorker(new StockMessageListener(connectionFactory, destination, start, stop, orders))).start());

        try {
            start.awaitUntilFullyBooted();
        } catch (InterruptedException e) {
            throw new RuntimeException("Booting failed");
        }
    }

    @Override
    public void start() {
        TransactionEngine transactionEngine = new TransactionEngine(new Processor(orders, new Matcher(new Counter()), stop));
        new Thread(new EngineProcess(transactionEngine)).start();
        new Thread(new ProcessingFinalizer(transactionEngine, processingListener, stop)).start();
    }

}
