Transaction Engine
=========================
**Description**

Simple initial implementation of stock transaction engine

**Compiling**

    $mvn clean install

**Configuration**
	
    -Dorg.slf4j.simpleLogger.defaultLogLevel=info
    -Dorg.slf4j.simpleLogger.log.com.gft.digitalbank.exchange.solution=debug 
    --log-file=log.txt

**Running**

    mvn clean test